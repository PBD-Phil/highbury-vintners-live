<?php

return [
    '*' => [
        'primaryDomain' => null,
        'redirectStatusCode' => 302,

        'sslRoutingBaseUrl' => "https://highburyvintners.co.uk/",
        'sslRoutingEnabled' => true,
        'sslRoutingRestrictedUrls' => ['/'],

        'maintenanceModeEnabled' => true,
        'maintenanceModePageUrl' => '/offline',
        'maintenanceModeAuthorizedIps' => ['::1', '127.0.0.1', '81.107.162.183', '86.145.251.66', '81.105.58.230', '82.1.38.87', '90.198.217.192'],
        'maintenanceModeResponseStatusCode' => 410,
    ],
    'dev' => [
        'sslRoutingEnabled' => false,
    ],
    'staging' => [
        'maintenanceModePageUrl' => null,
        'maintenanceModeResponseStatusCode' => 410,
    ],
    'production' => [
        'redirectStatusCode' => 301,
        'maintenanceModeResponseStatusCode' => 503,
    ]
];
