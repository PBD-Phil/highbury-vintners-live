//const searchClient = algoliasearch('91HFQMQ5PX', '4c853def810b206a8871f009e33cf4d0');
//const searchClient = algoliasearch('91HFQMQ5PX', '4c853def810b206a8871f009e33cf4d0');

const search = instantsearch({
  indexName: 'dev_products',
  searchClient: algoliasearch(
      '91HFQMQ5PX', 
      '4c853def810b206a8871f009e33cf4d0'
  ),
  // searchFunction(helper) {
  //   if (helper.state.query) {
  //     helper.search();
  //   }
  // }
});

console.log(search);

search.addWidgets([
  // instantsearch.widgets.searchBox({
  //   container: '#searchbox',
  // }),

  instantsearch.widgets.hits({
    container: '#hits',
    // item: document.getElementById('hit-card-template').innerHTML,
    // error: "Sorry, we didnt find any results for <em>\"{{ query }}\"</em>"
  }),

  instantsearch.widgets.refinementList({
    container: '#refinement-list',
    attribute: 'brand',
    operator: 'and',
    limit: 10,
    showMore: true,
    showMoreLimit: 20,
    searchable: true,
    searchablePlaceholder: 'Search our products',
    searchableIsAlwaysActive: false,
    searchableEscapeFacetValues: false,
    sortBy: ['count:desc', 'name:asc']
  }),

  instantsearch.widgets.sortBy({
    container: '#sort-by',
    items: [
      { label: 'Sort by Price (low to high)', value: 'dev_wine' },
      { label: 'Sort by Price (high to low)', value: 'dev_wine' },
      { label: 'Featured Products', value: 'dev_wine' },
    ],
  }),

  instantsearch.widgets.pagination({
    container: '#pagination',
  }),

  instantsearch.widgets.hitsPerPage({
    container: '#hits-per-page',
    items: [
      { label: 'Display 24 per page', value: 24, default: true },
      { label: 'Display 48 per page', value: 48 },
    ],
  })

]);

search.start();
