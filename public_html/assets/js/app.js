(function($, window, document, undefined){
	
	// $('.dropdown-trigger').dropdown();

	$('.modal').modal({
		endingTop: '35%'
	});
	$('select').formSelect();
	$('.tabs').tabs({
		swipeable: true
	});
	$('.carousel.carousel-slider').carousel({
		fullWidth: true,
		indicators: true
	});

	// $('#billingAddressSameAsShipping').on('click', function(){
	// 	if($('#billingAddressSameAsShipping').is("input:not(:checked)")) {
	// 		$('billingCollapseBody.collapsible-body').open(2);
	// 	} else {
	// 		$('billingCollapseBody.collapsible-body').close(2);
	// 	};
	// });

	console.log('app.js loaded');
	
})(jQuery, window, document);

document.addEventListener('DOMContentLoaded', function() {

	var userNav = document.getElementById('mobile-user-account-nav');
	var desktopLogo = document.getElementById('desktop-logo');
	var menuIcon = document.querySelector('.sidenav-trigger');
	var sideNav = document.querySelector('.sidenav.navigation');
	M.Sidenav.init(sideNav, {
		onOpenStart: function() {
			userNav.classList.add("mobile-nav-active");
			//menuIcon.classList.remove("sidenav-trigger");
			// menuIcon.classList.add("hide");
			// desktopLogo.classList.add("hide");
		},
		onOpenEnd: function() {
			menuIcon.classList.add("sidenav-close");
		},
		onCloseStart: function () {
			userNav.classList.remove("mobile-nav-active");
			//menuIcon.classList.remove("sidenav-close");
			// menuIcon.classList.remove("hide");
			// desktopLogo.classList.remove("hide");
		},
		onCloseEnd: function() {
			//menuIcon.classList.add("sidenav-trigger");
		}
	});

	var sideFilter = document.querySelector('.sidenav.filter');
	M.Sidenav.init(sideFilter, {
		edge: 'right'
	});

	var collapseElems = document.querySelectorAll('.collapsible');
	M.Collapsible.init(collapseElems, {
		accordion: false
	});

	var userDrop = document.querySelectorAll('.dropdown-trigger.user-dropdown');
    M.Dropdown.init(userDrop, {
		hover: false,
		alignment: 'bottom',
		constrainWidth: false
	});

	var cartDrop = document.querySelectorAll('.dropdown-trigger.cart-dropdown');
    M.Dropdown.init(cartDrop, {
		hover: false,
		constrainWidth: false
	});

	var eventsDrop = document.querySelectorAll('.dropdown-trigger.events-dropdown');
    M.Dropdown.init(eventsDrop, {
		hover: false,
		constrainWidth: false
	});

	var modalElems = document.querySelectorAll('.modal.gift-list-modal');
    var instances = M.Modal.init(modalElems, {
		startingTop: '1%',
		endingTop: '20%',
		inDuration: 750,
		outDuration: 500,
		opacity: 0.7
	});

	// Mobile navigation blocks
	var mobElOne = document.querySelectorAll('.sidenav.mobElOne');
	var instances = M.SidenavBlock.init(mobElOne, {
		edge: 'left'
	});

	var mobElTwo = document.querySelectorAll('.sidenav.mobElTwo');
	var instances = M.SidenavBlock.init(mobElTwo, {
		edge: 'left'
	});

});